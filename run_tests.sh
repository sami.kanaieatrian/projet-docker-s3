#!/bin/bash

# Initialisation du compteur de tests et d'erreurs
total_tests=4  # Modifier le nombre de tests ici
passed_tests=0

echo "Running generic tests..."

# Test 1 : Vérifier la présence d'un fichier README.md
if [ -f "README.md" ]; then
    echo "Test 1 - README.md exists: PASS"
    ((passed_tests++))
else
    echo "Test 1 - README.md exists: FAIL"
fi

# Test 2 : Vérifier si la commande 'echo' est disponible
if command -v echo &> /dev/null; then
    echo "Test 2 - 'echo' command available: PASS"
    ((passed_tests++))
else
    echo "Test 2 - 'echo' command available: FAIL"
fi

# Test 3 : Vérifier si le fichier .gitignore existe
if [ -f ".gitignore" ]; then
    echo "Test 3 - .gitignore exists: PASS"
    ((passed_tests++))
else
    echo "Test 3 - .gitignore exists: FAIL"
fi

# Test 4 : Vérifier que le script run_tests.sh est exécutable
if [ -x "run_tests.sh" ]; then
    echo "Test 4 - run_tests.sh is executable: PASS"
    ((passed_tests++))
else
    echo "Test 4 - run_tests.sh is executable: FAIL"
fi

# Afficher les résultats finaux
echo "Tests passed: $passed_tests/$total_tests"
if [ $passed_tests -eq $total_tests ]; then
    echo "All tests passed!"
    exit 0
else
    echo "Some tests failed."
    exit 1
fi
