from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .models import Article, Comment, Like
from django.core.cache import cache
from .forms import ArticleForm, CommentForm
from django.http import JsonResponse
import json


@login_required
def create_article(request):
    """
    Crée un nouvel article
    """
    if request.method == "POST":
        form = ArticleForm(request.POST)
        if form.is_valid():
            article = form.save(commit=False)
            article.author = request.user
            form.save()
            cache.set(f'article_{article.pk}', article, timeout=300)
            return redirect('blog:article_detail', pk=article.pk)
    else:
        form = ArticleForm()

    return_data = {
        'form': form
    }
    return render(request, 'article_form.html', return_data)


@login_required(login_url='blog:login')
def update_article(request, pk):
    """
    Met à jour un article
    """
    article = Article.objects.get(pk=pk)
    if request.method == "POST":
        form = ArticleForm(request.POST, instance=article)
        if form.is_valid():
            form.save()
            cache.set(f'article_{pk}', article, timeout=300)
            return redirect('blog:article_detail', pk=article.pk)
    else:
        form = ArticleForm(instance=article)

    return_data = {
        'form': form
    }
    return render(request, 'article_form.html', return_data)


def article_list(request):
    """
    Affiche la liste des articles
    """
    articles = Article.objects.all()
    return render(request, 'article_list.html', {'articles': articles})


def article_detail(request, pk, incr_views=True):
    """
    Affiche un article et ses commentaires

    :param request:
    :param pk: Clé primaire de l'article à afficher
    :param incr_views: Incrémenter le compteur de vues ou non
    """
    # Récupération de l'article à afficher
    article = cache.get(f'article_{pk}')
    if not article:
        article = Article.objects.get(pk=pk)
        cache.set(f'article_{pk}', article, timeout=300)

    # Création de commentaires
    comment_form = CommentForm()
    if request.method == 'POST':
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            new_comment = comment_form.save(commit=False)
            new_comment.article = article
            new_comment.author = request.user
            new_comment.save()

            comments = article.comments.all()
            cache.set(f'comments_for_{pk}', comments, timeout=300)
            return redirect('blog:article_detail_from_comment', pk=pk, incr_views=False)

    # Récupération des commentaires
    comments = cache.get(f'comments_for_{pk}')
    if not comments:
        comments = article.comments.all()
        cache.set(f'comments_for_{pk}', comments, timeout=300)
    for comment in comments:
        comment.likes = cache.get(f'comment_likes_{comment.pk}', 0)
        comment.dislikes = cache.get(f'comment_dislikes_{comment.pk}', 0)
        if request.user.is_authenticated:
            comment_user_like = Like.objects.filter(comment=comment, user=request.user).first()
            if comment_user_like:
                comment.user_like = comment_user_like.is_like

    views_key = f'article_views_{pk}'
    views = cache.get(views_key, 0)
    if incr_views:
        cache.set(views_key, views + 1, timeout=None)
    views = cache.get(views_key)

    return_data = {
        'article': article,
        'views': views,
        'comments': comments,
        'comment_form': comment_form,
    }
    return render(request, 'article_detail.html', return_data)


def like_dislike_comment(request):
    """
    Gère les likes et dislikes des commentaires
    """
    if request.method == 'POST' and request.user.is_authenticated:
        data = json.loads(request.body)
        comment_id = data.get('comment_id')
        action = data.get('action')
        if not comment_id:
            return JsonResponse({'error': 'Comment id is missing'}, status=400)
        if action is None or action not in ['like', 'dislike', 'cancel']:
            return JsonResponse({'error': 'Invalid action'}, status=400)

        comment = Comment.objects.get(pk=comment_id)
        try:
            like = Like.objects.get(comment=comment, user=request.user)
            if action == 'like':
                like.is_like = True
                like.save()
            elif action == 'dislike':
                like.is_like = False
                like.save()
            else:
                like.delete()
        except Like.DoesNotExist:
            if action == 'like':
                Like.objects.create(comment=comment, user=request.user, is_like=True)
            elif action == 'dislike':
                Like.objects.create(comment=comment, user=request.user, is_like=False)
            else:
                return JsonResponse({'error': 'Invalid action'}, status=400)

        cache.set(f'comment_likes_{comment.pk}', comment.get_likes_count(), timeout=None)
        cache.set(f'comment_dislikes_{comment.pk}', comment.get_dislikes_count(), timeout=None)

        likes_count = comment.get_likes_count()
        dislikes_count = comment.get_dislikes_count()

        return JsonResponse({'likes_count': likes_count, 'dislikes_count': dislikes_count})

    return JsonResponse({'error': 'Invalid request'})


def search_article(request):
    """
    Recherche un article
    """
    if request.method == 'POST':
        search_term = request.POST.get('search_term')
        articles = Article.objects.filter(title__icontains=search_term)
        messages.add_message(
            request, messages.INFO,
            'Voici les articles correspondant à votre recherche')
        return render(request, 'article_list.html',
                      {'articles_filtered': articles})

    return render(request, 'article_list.html', {'articles': []})