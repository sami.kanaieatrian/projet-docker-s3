#!/bin/bash
# run_integration_tests.sh

echo "Running integration tests..."

# Exécuter les tests d'intégration avec pytest
pytest -q tests/integration/test_calculator_integration.py --cov=src --cov-report=xml --cov-report=term-missing --cov-fail-under=80

# Vérifier si pytest a réussi
if [ $? -ne 0 ]; then
    echo "Integration tests failed!"
    exit 1
else
    echo "Integration tests passed!"
    exit 0
fi
