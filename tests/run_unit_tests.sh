#!/bin/bash
# run_unit_tests.sh

echo "Running unit tests..."

# Exécuter les tests unitaires avec pytest
pytest -q tests/unit/test_calculator_unit.py --cov=src --cov-report=xml --cov-report=term-missing --cov-fail-under=80

# Vérifier si pytest a réussi
if [ $? -ne 0 ]; then
    echo "Unit tests failed!"
    exit 1
else
    echo "Unit tests passed!"
    exit 0
fi
