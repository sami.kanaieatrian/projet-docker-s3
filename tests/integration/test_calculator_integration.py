# test_calculator_integration.py

from src.calculator import add, multiply, divide

def test_integration_operations():
    result = add(10, 5)
    result = multiply(result, 2)
    result = divide(result, 3)
    assert result == 10  # (10 + 5) * 2 / 3 == 10
