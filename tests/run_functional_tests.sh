#!/bin/bash
# run_functional_tests.sh

echo "Running functional tests..."

# Exécuter les tests fonctionnels avec pytest
pytest -q tests/functional/test_calculator_functional.py --cov=src --cov-report=xml --cov-report=term-missing --cov-fail-under=80

# Vérifier si pytest a réussi
if [ $? -ne 0 ]; then
    echo "Functional tests failed!"
    exit 1
else
    echo "Functional tests passed!"
    exit 0
fi
