# test_calculator_functional.py

from src.calculator import add, subtract, multiply, divide

def test_functional_calculation():
    result = add(10, 5)
    result = subtract(result, 3)
    result = multiply(result, 2)
    result = divide(result, 6)
    assert result == 4  # ((10 + 5 - 3) * 2) / 6 == 4
