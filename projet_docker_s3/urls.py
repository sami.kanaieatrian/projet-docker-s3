
from django.contrib import admin
from django.urls import path, include

API_PREFIX = 'projet_docker_s3/'

urlpatterns = [
    path(API_PREFIX+'admin/', admin.site.urls),  # admin
    path(API_PREFIX+'blog/', include('blog.urls')),  # blog
    path(API_PREFIX+'auth/', include('authentication.urls')),  # blog
]
