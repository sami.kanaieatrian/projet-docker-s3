# calculator.py

"""
Calculator module containing basic arithmetic functions.
"""

def add(a, b):
    """
    Adds two numbers together.
    
    Args:
        a (int or float): The first number.
        b (int or float): The second number.

    Returns:
        int or float: The sum of the two numbers.
    """
    return a + b

def subtract(a, b):
    """
    Subtracts the second number from the first.
    
    Args:
        a (int or float): The first number.
        b (int or float): The second number.

    Returns:
        int or float: The result of the subtraction.
    """
    return a - b

def multiply(a, b):
    """
    Multiplies two numbers together.
    
    Args:
        a (int or float): The first number.
        b (int or float): The second number.

    Returns:
        int or float: The product of the two numbers.
    """
    return a * b

def divide(a, b):
    """
    Divides the first number by the second.
    
    Args:
        a (int or float): The numerator.
        b (int or float): The denominator. Must not be zero.

    Returns:
        float: The quotient of the two numbers.
        
    Raises:
        ValueError: If the denominator is zero.
    """
    if b == 0:
        raise ValueError("Cannot divide by zero")
    return a / b
